//
//  CDNAutomatedVideosModel.swift
//  dynamo
//
//  Created by Muhammad Junaid on 23/09/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit
import AWSDynamoDB


class CDNAutomatedVideosModel: AWSDynamoDBObjectModel, AWSDynamoDBModeling  {
    // MARK: - Properties
    @objc var guid: String?
    @objc var hlsUrl: String?
    @objc var mp4Urls: NSSet?
    
    func getMp4AsArray() -> [String] {
        return mp4Urls?.allObjects as? [String] ?? []
    }
    
    // MARK: - AWSDynamoDBObjectModel, AWSDynamoDBModeling
    class func dynamoDBTableName() -> String {
        return "CDNAutomatedVideos"
    }
    
    class func hashKeyAttribute() -> String {
        return "guid"
    }
}
