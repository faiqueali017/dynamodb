//
//  ViewController.swift
//  dynamo
//
//  Created by Faique Ali on 23/09/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit
import AWSDynamoDB
import AWSCore

class ViewController: UIViewController {

    /**
     print 'hlsUrl' and 'mp4Urls' in debug area of every search made
     
     searching hardcoded values concat in 'srcVideo' as below:
     srcVideo string format: "PostProducedVideos/XXXXXXX.mp4"
     
     Hardcoded values:
     
     Grade2MoneyTypesOfCurrency
     Grade2MathsTimeAmPm
     Grade1UrduChotiYaayKiAwaazChotiYaayKiAwaaz
     Grade6EnglishNounsRegularAndIrregularNouns
     
     */
    
    var fileName = "Grade1EnglishTwoAndThreeLetterSoundsShChPh"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        configuringDB()
        loadVideo()
    }

    //MARK:- Helper Methods
    //configure table
    private func configuringDB(){
        let dynamoDB = AWSDynamoDB.default()
        let listTableInput = AWSDynamoDBListTablesInput()
        dynamoDB.listTables(listTableInput!).continueWith{(task: AWSTask<AWSDynamoDBListTablesOutput>) -> Any? in
            if let error = task.error as? NSError {
                print("Error occured: \(error)")
                return nil
            }
            
            let listTablesOutput = task.result
            
            for tableName in listTablesOutput!.tableNames! {
                print("\(tableName)")
            }
            return nil
        }
    }
    
    
    func loadVideo() {
        let objectMapper = AWSDynamoDBObjectMapper.default()
        let queryExpression = AWSDynamoDBQueryExpression()
        queryExpression.indexName = "srcVideo-index"
        queryExpression.keyConditionExpression = "#srcVideo = :srcVideo"
        queryExpression.expressionAttributeNames = ["#srcVideo": "srcVideo",]
        queryExpression.expressionAttributeValues = [":srcVideo" : "PostProducedVideos/\(fileName).mp4"]
        objectMapper.query(CDNAutomatedVideosModel.self, expression: queryExpression).continueWith(executor: .mainThread(), block: { (task) in
            
            if let result = task.result {
                if let responseModel = result.items as? [CDNAutomatedVideosModel] {
                    if let firstObject = responseModel.first {
                        print("HLS URL: " + firstObject.hlsUrl!)
                        let mp4Urls = firstObject.getMp4AsArray()
                        print("MP4 URL: \(mp4Urls[0])")
                        
                    }
                }
            }
            
            else if let error = task.error {
                print(error)
            }
            
            return nil
        })
    }
}

